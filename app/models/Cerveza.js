const connection = require('../config/dbconnection.js')

const all = function (callback) {
  const sql = 'SELECT * FROM cervezas'
  connection.query(sql, (err, result, fields) => {
    if (err) {
      callback(500, err)
      console.log('hubo un error' + err)
    } else {
      callback(200, result, fields)
    }
  })
}

const find = function (id, callback) {
  const sql = 'SELECT * FROM cervezas WHERE id = ?' // de este modo evitas alterar el sql (el ?)
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      callback(500, err)
      console.log('hubo un error' + err)
    } else {
      callback(200, result, fields)
    }
  })
}

const create = function (cerveza, callback) {
  const sql =
    'INSERT INTO cervezas(name, container, alcohol, price)' +
    ' VALUES(?, ?, ?, ?)'
  connection.query(
    sql,
    [cerveza.name, cerveza.container, cerveza.alcohol, cerveza.price],
    (err, result, fields) => {
      if (err) {
        callback(500, err)
        console.log('hubo un error' + err)
      } else {
        callback(200, result, fields)
      }
    }
  )
}

const destroy = function (id, callback) {
  const sql = 'DELETE FROM cervezas WHERE id = ?' // de este modo evitas alterar el sql
  connection.query(sql, [id], (err, result, fields) => {
    if (err) {
      callback(500, err)
      console.log('hubo un error' + err)
    } else {
      callback(200, result, fields)
    }
  })
}

const update = function (cerveza, callback) {
  const sql =
    'UPDATE cervezas SET name=?, container=?, alcohol=?, price=? WHERE id = ?' // de este modo evitas alterar el sql
  connection.query(
    sql,
    [
      cerveza.id,
      cerveza.name,
      cerveza.container,
      cerveza.price,
      cerveza.alcohol
    ],
    (err, result, fields) => {
      if (err) {
        callback(500, err)
        console.log('hubo un error' + err)
      } else {
        callback(200, result, fields)
      }
    }
  )
}

module.exports = {
  all,
  find,
  create,
  destroy,
  update
}
