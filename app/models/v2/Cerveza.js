const mongoose = require('mongoose')
const Schema = mongoose.Schema

const cervezaSchema = new Schema({
  // la estructura para crear los objetos
  nombre: {
    type: String,
    required: true
  },
  descripción: String,
  graduación: String,
  envase: String,
  precio: String
})

const Cerveza = mongoose.model('Cerveza', cervezaSchema)

module.exports = Cerveza
