const mongoose = require('mongoose')
const Schema = mongoose.Schema
const User = require('./User.js')

const orderSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  fecha: {
    type: Date,
    default: Date.now
  },
  productos: {
    type: Schema.Types.ObjectId,
    ref: 'Product'
  }
})

const Order = mongoose.model('Order', orderSchema)

module.exports = Order
