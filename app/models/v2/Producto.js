const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productoSchema = new Schema({
  // la estructura para crear los objetos
  name: {
    type: String,
    maxLength: 20,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  descripcion: {
    type: String,
    maxLength: 255
  },
  created: {
    // no tenemos que introducirla
    type: Date,
    default: Date.now
  }
})

const Producto = mongoose.model('Producto', productoSchema) // guardamos el modelo

module.exports = Producto
