const express = require('express')
const router = express.Router()
// creamos el enrutador y a continuación le damos una ruta

const orderController = require('../../controllers/v2/orderController')
// las rutas que pongamos nos llevarán a los métodos del controlador

router.get('/', (req, res) => {
  // index
  orderController.index(req, res)
})

router.get('/:id', (req, res) => {
  // show con el id
  orderController.show(req, res)
})

router.post('/', (req, res) => {
  orderController.create(req, res)
})

router.delete('/:id', (req, res) => {
  orderController.remove(req, res)
})

router.put('/:id', (req, res) => {
  orderController.update(req, res)
})

module.exports = router
