const express = require('express')

const router = express.Router()
// creamos el enrutador y a continuación le damos una ruta

const cervezaController = require('../../controllers/v2/cervezaController.js')
// las rutas que pongamos nos llevarán a los métodos del controlador

router.get('/', (req, res) => {
  // index
  cervezaController.index(req, res)
})

router.get('/search', (req, res) => {
  // search con el nombre
  cervezaController.search(req, res)
})

router.get('/:id', (req, res) => {
  // show con el id
  cervezaController.show(req, res)
})

router.post('/', (req, res) => {
  cervezaController.create(req, res)
})

router.delete('/:id', (req, res) => {
  cervezaController.remove(req, res)
})

router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
})

module.exports = router
