const express = require('express')
const router = express.Router()
const userController = require('../../controllers/v2/userController')

router.get('/', (req, res) => {
  userController.index(req, res)
})

router.post('/', userController.register)

module.exports = router
