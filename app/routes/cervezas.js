const express = require('express')

const router = express.Router()
// creamos el enrutador y a continuación le damos una ruta

const cervezaController = require('../controllers/cervezaController.js')

router.get('/', (req, res) => {
  cervezaController.index(req, res)
})

// router.get('/', (req, res) => {
//     let page = req.query.page ? req.query.page : 1;
//     page = req.query.page || 1;
//     // se puede usar una u otra. Si hay página, cogemos esa, sino, es la 1.
//     res.json({ mensaje: `Lista de cervezas, página ${page}` })
// })

router.get('/:id', (req, res) => {
  cervezaController.show(req, res)
})

router.post('/', (req, res) => {
  cervezaController.store(req, res)
})

router.delete('/:id', (req, res) => {
  cervezaController.destroy(req, res)
})

router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
})

// router.get('/:id', (req, res) => {
//   cervezaController.json({ mensaje: `¡Cerveza número: ${req.params.id}!` })
// })

// router.post('/', (req, res) => {
//   res.json({ mensaje: `¡Cerveza creada! ${req.body.nombre}` })
// })

// router.delete('/', (req, res) => {
//   res.json({ mensaje: '¡Cerveza borrada!' })
// })

// router.put('/', (req, res) => {
//   res.json({ mensaje: '¡Cerveza actualizada!' })
// })

module.exports = router
