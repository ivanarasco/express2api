const express = require('express');

const router = express.Router();
const routerCervezas = require('./routes/cervezas.js') // creamos una variable que es de esta ruta.

router.get('/', (req, res) => {
    res.json({ mensaje: '¡Bienvenido a nuestra Api!' })
 })

 router.use('/cervezas', routerCervezas); // esta es la ruta asignada para acceder ahí (line 4)

 module.exports = router
 