const express = require('express')

const router = express.Router()
const routerCervezas = require('./routes/v2/cervezas.js')
const routerProductos = require('./routes/v2/productos.js')
const routerUsers = require('./routes/v2/users.js')

const routerPedidos = require('./routes/v2/orders.js')

router.use('/cervezas', routerCervezas)
router.use('/productos', routerProductos)
router.use('/users', routerUsers)

router.use('/orders', routerPedidos)
module.exports = router
